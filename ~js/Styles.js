
window.defaultStyle = {
    questionColor: [0,0,0],
    textColor: [255, 255, 255],
    backgroundColor: [255, 255, 255],
//    gradientBackground: [240, 240, 240],
//        color1: [255, 45, 45],
    color1: [255, 105, 97],
//        color2: [70, 140, 255],
    color2: [80, 150, 255],
    color3: [140, 180, 70],
    color4: [255, 180, 70],
    //    percent: [255, 255, 0],
    percent: [230, 230, 230],
    extraValues: ["nm", "from"]
};
