
function DrawPoll(values) {
    for (var key in values) {
        this[key] = values[key];
    }

    this.style = {};
    if ("object" === typeof this.obj.style) {
        this.style = this.obj.style;
    }
    this.question = this.obj.question;
    this.options = this.obj.options;

    this.maxOptions = 4; //for twitter mobile visualization
}

DrawPoll.prototype.drawCanvas = function (callback) {
    var w = this.width;

    //LOAD INFO:
    var quest = this.question;
    var opts = this.options;
    var url = this.url;
    var country = this.country;

    //LOAD SYLE
    for (var key in window.defaultStyle) {
        if (!this.style[key]) {
            this.style[key] = window.defaultStyle[key];
        }
    }

    //START CANVAS
    this.draw = new Draw(w);
    this.canvas = this.draw.canvas;
    this.ctx = this.draw.ctx;
    var ctx = this.ctx;

    //cast before drawOption loop
    this.totalVotes = 0;
    for (var i = 0; i < opts.length; i++) {
        this.totalVotes += +opts[i][2];
    }

    this.h = 0;

    //BACKGROUND
    this.draw.background(this.style.backgroundColor.toString());

    //OWNER
    //this.style.owner = "prueba"
    if (this.style.owner) {
        this.draw.owner(this.style.owner, w * 0.03, this.style.questionColor.toString());
        this.h += parseInt(w / 17);
    }

    //TITLE
    //quest = "Pregunta de prueba";
    if (quest) {
        if (this.h < 1) {
            this.h += parseInt(w / 19);
        }
        this.h += parseInt(w / 63);

        ctx.font = parseInt(w / 23) + "px Arial Narrow";
        ctx.textAlign = "left";
        this.draw.title(quest, w * 0.015, this.h, this.style.questionColor.toString());
    }

    //if no owner and title allow 5 answers!
    if (0 == this.h) {
        this.maxOptions++;
    }

    this.lineHeight = w * 0.077;
    this.optionMargin = w * 0.007;
    //ADD MARGINS ON FEW ANSWERS
    var diff = Math.max(4 - opts.length, 0);
    if (diff) {
        this.h += diff * w * 0.005;
        this.lineHeight += diff * w * 0.026;
        this.optionMargin += diff * w * 0.007;
    }

    //OPTIONS (AND HEIGHT CALCULATION)
    for (var i = 0; i < opts.length; i++) {
        //limit options on image
        if (i == this.maxOptions) {
            break;
        }
        var name = opts[i][1];
        var val = opts[i][2];
        this.drawOption(name, val, i, "LeagueGothic");
    }
//    //DEBUG
//    for (var i = 0; i < opts.length; i++) {
//        var name = opts[i][1];
//        var val = opts[i][2];
//        this.drawOption(name, val, i + 2, "LeagueGothic");
//    }
//    var name = opts[0][1];
//    var val = opts[0][2];
//    this.drawOption(name, val, 4, "LeagueGothic");

    // 'more'
    if (this.maxOptions < this.options.length) {
        this.draw.more();
    }
    
    if (!this.show) {
        this.draw.clickHere("Hand");
    }

    //footer last -> be sure url is visible
    var _this = this;
    this.draw.footer(url, w * 0.01, 0, country, function () {
        //w8 canvas footer images..
        callback(_this.canvas);
    });
};

DrawPoll.prototype.drawOption = function (optionName, value, pos, font) {
    //console.log(optionName + " : " + value);
    var ctx = this.ctx;
    var w = this.width;
    var optionHeight = pos * (this.lineHeight + this.optionMargin);

    //SQUARE:
    var width_all = w - 11;
    //var height_all = w * 0.071;
    var height_all = this.lineHeight;
    var color = this.style["color" + (pos + 1)];
    if (!color) {
        color = [100, 100, 100];
    }
    this.draw.optionSquare(5, this.h + 5 + optionHeight, width_all, height_all, color.toString());

    var percent = 0;
    if (this.totalVotes) {
        percent = value / this.totalVotes;
    }

    //CHECK WILL SHOW VOTES
    this.show = false;
    if (this.obj.users && this.obj.users[window.user.id]) {
        var userVotes = this.obj.users[window.user.id][1];
        if ("undefined" !== typeof userVotes && "" !== userVotes) {
            this.show = true;
        } else {
            //console.log("NOT USER IN POLL: '" + window.user.id + "'");
        }
    }

    //not add percent background on 100%
    if (percent != 0 && percent != 1 && this.show) {
        //background
        var gradientWidth = w - 12;

        //OPTION GRADIENTS
        this.draw.gradient(percent, 5, this.h + 5 + optionHeight, gradientWidth, height_all, "255,255,255", "h");
    }

    //var altura = this.h + this.lineHeight * 0.96 + pos * this.lineHeight;
    var altura = this.h + 20 + optionHeight + this.lineHeight / 2;

    //OPTION
    optionName = decode_uri(optionName);
//    optionName = optionName.split("").join(String.fromCharCode(8202)); //this cause bad width calculations on multiple-line text!!
    ctx.font = parseInt(w / 17) + "px " + font;
    ctx.textBaseline = "middle";
    ctx.textAlign = "left";
    var left = this.draw.option(optionName, 15, altura, this.style.textColor.toString(), w - 30);

    //END
    if (!this.totalVotes || !this.show) {
        return;
    }

    //PERCENTAGE:
    ctx.textAlign = "left";
    ctx.font = parseInt(w / 22) + "px LeagueGothic";
    this.draw.percentage(percent, 20 + left, altura, this.style.percent.toString());

    //VOTES:
    var textValue = formatNumber(value);
    ctx.textBaseline = "bottom";
    ctx.font = parseInt(this.width / 26) + "px LeagueGothic";
    this.draw.votes(textValue, -15, altura - (this.lineHeight / 4), "gainsboro");
};
