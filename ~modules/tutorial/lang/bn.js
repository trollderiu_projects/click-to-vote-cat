$.extend(window.lang_bn, {
"help_welcome": "টিউটোরিয়াল স্বাগতম: আপনি বরং ?, <b> ভাগ! </ B> জন্য <br> এই টিউটোরিয়ালটি আপনাকে কয়েকটি সহজ পদক্ষেপে <b> কিভাবে ব্যবহার করতে হবে </ b> এই অ্যাপটি দেখাবে!",
"help_first": "প্রথমে, <b> প্রথম </ b> বিকল্পে <b> সন্নিবেশ করান </ b> পাঠ্য",
"help_second": "তারপর, <b> দ্বিতীয় </ b> বিকল্পে <b> সন্নিবেশ করান </ b> পাঠ্য।",
"help_share": "এবং <b> ভাগ করুন </ b> বোতামে ক্লিক করুন!",
"help_shareDone": "এবং আপনি <b> যে কোনও </ b> <b> ভাগ </ b> করতে পারেন!",
"help_playFunctionality": "আপনি যদি <b> Play </ b> <b> খেলা </ b> করতে চান তবে আপনি বরং ...",
"help_play": "শিরোলেখটির <b> প্লে করুন </ b> বোতামে ক্লিক করুন ...",
"help_playScreen": "<B> শুরু </ b> <b> শুরু </ b> করুন! <br> <br> (শিরোনামের 'নতুন' বোতামে ক্লিক করুন আপনার ভোট ভাগ করার জন্য ফিরে যান)",
"help_end": "<B> সমাপ্ত </ b> <b> টিউটোরিয়াল </ b> :) এর জন্য আপনাকে ধন্যবাদ",
});