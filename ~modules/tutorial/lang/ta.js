$.extend(window.lang_ta, {
"help_welcome": "<B> பகிர்! </ B> க்கான, உங்களுக்குப் பதிலாக? <br> இந்த டுடோரியலை <b> எப்படி பயன்படுத்துவது </ b> இந்த பயன்பாட்டை சில எளிய வழிமுறைகளில் காண்பிக்கும்!",
"help_first": "முதலில், <b> முதல் </ b> விருப்பத்தில் <b> செருக </ b> உரை.",
"help_second": "பின்னர், <b> இரண்டாவது </ b> விருப்பத்தில் <b> செருக </ b> உரை.",
"help_share": "<B> பகிர் </ b> பொத்தானைக் கிளிக் செய்க!",
"help_shareDone": "<B> பகிர் </ b> <b> எங்கிருந்தும் </ b>!",
"help_playFunctionality": "<B> Play </ b> <b> கேம் </ b> என்ற விருப்பத்தை நீங்கள் விரும்பினாலும் ...",
"help_play": "தலைப்பில் <b> Play </ b> பொத்தானைக் கிளிக் செய்க ...",
"help_playScreen": "<B> விளையாட </ b> <b> ஐத் தொடங்கு </ b>! <br> <br> (உங்கள் வாக்கெடுப்பைப் பகிர்ந்து கொள்ள, மீண்டும் செல்ல, 'புதிய' பொத்தானை கிளிக் செய்யவும்)",
"help_end": "<B> tutorial </ b> <b> டுடோரியல் </ b> க்கு நன்றி",
});