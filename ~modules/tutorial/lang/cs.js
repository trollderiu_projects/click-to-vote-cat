$.extend(window.lang_cs, {
"help_welcome": "Vítejte v tutoriálu: Byl byste spíše ?, pro <b> sdílet! </ B>. <br> Tento návod vám ukáže <b> použití </ b> této aplikace v několika jednoduchých krocích!",
"help_first": "Nejprve vložte <b> vložte </ b> text do volby <b> první </ b>.",
"help_second": "Potom vložte <b> Insert </ b> text na volbu <b> druhé </ b>.",
"help_share": "A klikněte na tlačítko <b> sdílet </ b>!",
"help_shareDone": "A můžete <b> sdílet </ b> to <b> kdekoli </ b>!",
"help_playFunctionality": "Pokud chcete také <b> Přehrát </ b> <b> Hra </ b> byste chtěli ...",
"help_play": "Klikněte na tlačítko <b> Přehrát </ b> v záhlaví ...",
"help_playScreen": "A <b> začněte </ b> a <b> přehrát </ b>! <br> <br> (Klikněte na tlačítko 'nové' v záhlaví, abyste se vrátili a sdíleli své ankety)",
"help_end": "Děkujeme vám za <b> dokončení </ b> <b> tutoriálu </ b> :)",
});