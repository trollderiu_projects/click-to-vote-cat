$.extend(window.lang_mt, {
"help_welcome": "Merħba fil-tutorja ta ': Intom aktar ?, għal <b> sehem! </ B>. <br> Dan it-tutorja juruk <b> kif tuża </ b> din l-App fi ftit passi sempliċi!",
"help_first": "L-ewwel, <b> inserixxi </ b> test fuq l-ewwel <b> </ b>.",
"help_second": "Imbagħad, <b> Daħħal </ b> test fuq it-tieni <b> </ b>.",
"help_share": "U kklikkja fuq buttuna <b> sehem </ b>!",
"help_shareDone": "U int tista '<b> sehem </ b> hija <b> kullimkien </ b>!",
"help_playFunctionality": "Jekk inti tixtieq ukoll <b> Play </ b> il-logħba <b> </ b> ta 'Intom pjuttost ...",
"help_play": "Ikklikja fuq buttuna <b> Play </ b> fuq l-intestatura ...",
"help_playScreen": "U <b> tibda </ b> biex <b> play </ b>! <br> <br> (Ikklikkja fuq buttuna 'ġdida' fuq l-intestatura biex tmur lura biex taqsam l-istħarriġ tiegħek)",
"help_end": "Grazzi għal <b> finitura </ b> il-<b> tutorja </ b> :)",
});