$.extend(window.lang_eu, {
"help_welcome": "Ongi etorri Tutoriala: Nahi al duzu ?, <b> partekatu! </ B>. <br> Tutorial honek <b> nola erabili </ b> aplikazioa erakutsiko dizu urrats bakun batzuetan!",
"help_first": "Lehenik eta behin, <b> txertatu </ b> testua <b> lehen </ b> aukera.",
"help_second": "Ondoren, <b> Txertatu </ b> testua <b> bigarren </ b> aukeran.",
"help_share": "Eta egin klik <b> partekatu </ b> botoian!",
"help_shareDone": "<B> partekatu </ b> <b> edonon </ b> egin dezakezu!",
"help_playFunctionality": "<B> Play </ b> <b> Jokoan </ b> nahi baduzu, nahiago baduzu ...",
"help_play": "Egin klik <b> Erreproduzitu </ b> botoian goiburuko ...",
"help_playScreen": "Eta <b> hasi </ b> <b> jolastu </ b>! <br> <br> (Egin klik goiburuko 'berri' botoian berriro itzultzeko zure galdeketarako)",
"help_end": "Eskerrik asko <b> amaitzeko </ b> <b> tutorial </ b> :)",
});