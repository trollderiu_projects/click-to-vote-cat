$.extend(window.lang_ms, {
"help_welcome": "Selamat datang ke tutorial: Adakah anda Sebaliknya ?, untuk <b> kongsi! </ B>. <br> Tutorial ini akan menunjukkan kepada anda <b> cara menggunakan </ b> App ini dalam beberapa langkah mudah!",
"help_first": "Pertama, <b> masukkan </ b> teks pada pilihan <b> pertama </ b>.",
"help_second": "Kemudian, <b> Sisipkan </ b> teks pada pilihan <b> kedua </ b>.",
"help_share": "Dan klik butang <b> kongsi </ b>!",
"help_shareDone": "Dan anda boleh <b> berkongsi </ b> ia <b> mana-mana </ b>!",
"help_playFunctionality": "Jika anda juga ingin <b> Main </ b> <b> Permainan </ b> daripada Adakah anda Lebih ...",
"help_play": "Klik pada butang <b> Main </ b> pada pengepala ...",
"help_playScreen": "Dan <b> mulakan </ b> ke <b> main </ b>! <br> <br> (Klik pada butang 'baru' di pengepala untuk kembali untuk berkongsi tinjauan anda)",
"help_end": "Terima kasih kerana <b> selesai </ b> <b> tutorial </ b> :)",
});