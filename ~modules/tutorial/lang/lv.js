$.extend(window.lang_lv, {
"help_welcome": "Laipni lūdzam apmācībā par: Vai jūs tā vietā? <B> koplietojiet! </ B>. <br> Šī apmācība parādīs jums <b> kā lietot </ b> šo programmu dažās vienkāršās darbībās!",
"help_first": "Pirmkārt, <b> ievietojiet </ b> tekstu <b> pirmajā </ b> opcijā.",
"help_second": "Pēc tam <b> Ievietojiet </ b> tekstu opcijā <b> otrā </ b>.",
"help_share": "Noklikšķiniet uz pogas <b> Koplietot </ b>!",
"help_shareDone": "Un jūs varētu <b> kopīgot </ b> to <b> jebkurā vietā </ b>!",
"help_playFunctionality": "Ja vēlaties arī <b> Atskaņot </ b> <b> spēli </ b>, vai jūs drīzāk ...",
"help_play": "Noklikšķiniet uz pogas <b> Atskaņot </ b> uz galvenes ...",
"help_playScreen": "Un <b> sākt </ b>, lai <b> spēlēt </ b>! <br> <br> (Noklikšķiniet uz pogas 'Jauns', lai atgrieztos, lai dalītos ar aptaujām)",
"help_end": "Paldies par <b> apdari </ b> <b> apmācība </ b> :)",
});