$.extend(window.lang_ro, {
"help_welcome": "Bine ați venit la tutorialul 'Vrei mai degrabă?', Pentru <b> partajați-vă! </ B>. <br> Acest tutorial vă va arăta <b> cum să utilizați </ b> acest App în câțiva pași simpli!",
"help_first": "Mai întâi, <b> introduceți </ b> textul în opțiunea <b> prima </ b>.",
"help_second": "Apoi, <b> Introduceți </ b> textul în opțiunea <b> secundă </ b>.",
"help_share": "Și faceți clic pe butonul <b> partajați </ b>!",
"help_shareDone": "Și ați putea <b> să o distribuiți <b> oriunde </ b>!",
"help_playFunctionality": "Dacă doriți, de asemenea, să <b> Redați </ b> <b> Jocul </ b> de mai ...",
"help_play": "Faceți clic pe butonul <b> Redare </ b> din antetul ...",
"help_playScreen": "Și <b> începeți </ b> pentru a <b> juca </ b>! <br> <br> (Faceți clic pe butonul 'nou' pe antet pentru a vă întoarce pentru a vă distribui sondajele)",
"help_end": "Vă mulțumim pentru <b> terminați </ b> tutorialul <b> </ b> :)",
});