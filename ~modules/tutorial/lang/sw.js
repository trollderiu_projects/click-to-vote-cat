$.extend(window.lang_sw, {
"help_welcome": "Karibu kwenye mafunzo ya: Ungependa ?, kwa <b> kushiriki! </ B>. <br> Mafunzo haya yatakuonyesha <b> jinsi ya kutumia </ b> Programu hii katika hatua chache rahisi!",
"help_first": "Kwanza, <b> ingiza </ b> maandishi juu ya chaguo <b> kwanza </ b>.",
"help_second": "Kisha, <b> Ingiza </ b> maandishi kwenye chaguo <b> pili </ b>.",
"help_share": "Na bonyeza kifungo <b> kushiriki </ b>!",
"help_shareDone": "Na unaweza <b> kushiriki </ b> it <b> popote </ b>!",
"help_playFunctionality": "Ikiwa unataka pia <b> Jaribu </ b> <B> Game </ b> ya Je, ungependa ...",
"help_play": "Bonyeza kifungo <b> Kutafuta </ ​​b> kwenye kichwa ...",
"help_playScreen": "Na <b> kuanza </ b> kwa <b> kucheza </ b>! <br> <br> (Bofya kwenye kifungo 'kipya' juu ya kichwa ili kurudi kwa kushiriki kura zako)",
"help_end": "Asante kwa <b> kumaliza </ b> mafunzo <b> </ b> :)",
});