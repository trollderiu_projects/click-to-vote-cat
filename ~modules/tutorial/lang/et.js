$.extend(window.lang_et, {
"help_welcome": "Tere tulemast tutvustamisse: kas peaksite selle asemel <b> jagama! </ B>. <br> See õpetus näitab <b> kuidas seda </ b> seda rakendust kasutada mõne lihtsa sammuga!",
"help_first": "Esmalt <b> sisesta </ ​​b> tekst <b> esmalt </ b>.",
"help_second": "Seejärel <b> Lisa </ b> tekst <b> sekundi </ b> valik.",
"help_share": "Ja klõpsake nuppu <b> Jagamine </ b>!",
"help_shareDone": "Ja võite <b> jagada </ b> seda <b> kõikjal </ b>!",
"help_playFunctionality": "Kui soovite ka <b> Esita </ ​​b> <b> mäng </ b>, kas peaksite selle asemel ...",
"help_play": "Klõpsake päises <b> Esita </ ​​b> päises ...",
"help_playScreen": "Ja <b> alusta </ ​​b>, et <b> mängida </ b>! <br> <br> (Kliki päises olevale nupule 'uus', et minna tagasi oma küsitluste jagamiseks)",
"help_end": "Täname <b> lõpetama </ b> <b> juhendaja </ b> :)",
});