$.extend(window.lang_lt, {
"help_welcome": "Sveiki atvykę į mokomąją medžiagą apie tai, ar jūs, o ne <b> bendrinkite </ b>. <br> Ši instrukcija parodys <b> kaip naudotis </ b> šia programa keliais paprastais žingsniais!",
"help_first": "Pirma, <b> įterpti </ b> tekstą į parinktį <b> pirmoji </ b>.",
"help_second": "Tada <b> Įterpti </ b> tekstą <b> antroje </ b>.",
"help_share": "Ir spustelėkite mygtuką <b> Pasidalinti </ b>!",
"help_shareDone": "Ir jūs galite <b> bendrinti </ b> <b> bet kur </ b>!",
"help_playFunctionality": "Jei taip pat norėtumėte <b> žaisti </ b> <b> žaidimą </ b> iš arčiau ...",
"help_play": "Spustelėkite <b> Groti </ b> mygtuką antraštėje ...",
"help_playScreen": "Ir <b> Pradėti </ b>, kad <b> paleisti </ b>! <br> <br> (Paspauskite 'new' mygtuką antraštėje grįžti pasidalinti savo apklausomis)",
"help_end": "Dėkojame, kad <b> baigė </ b> <b> tutorial </ b> :)",
});