$.extend(window.lang_sl, {
"help_welcome": "Dobrodošli v tutorialu: Ali bi radi ?, za <b> delite! </ B>. <br> Ta vadnica vam bo pokazala <b> kako uporabljati </ b> to aplikacijo v nekaj preprostih korakih!",
"help_first": "Najprej vstavite besedilo <b> vstavite </ b> v <b> prvo </ b>.",
"help_second": "Nato besedilo <b> Vstavi </ b> v <b> drugo </ b>.",
"help_share": "In kliknite gumb <b> deljenje </ b>!",
"help_shareDone": "In lahko <b> delite </ b> <b> kjerkoli </ b>!",
"help_playFunctionality": "Če želite tudi, da <b> Play </ b> od <b> Game </ b> od You Would Between ...",
"help_play": "V glavi kliknite gumb <b> Predvajaj </ b> ...",
"help_playScreen": "In <b> začni </ b> do <b> predvajaj </ b>! <br> <br> (Kliknite na gumb 'novo' na glavi, da se vrnete, da boste delili svoje ankete)",
"help_end": "Zahvaljujemo se vam za <b> zaključek </ b> <b> Tutorial </ b> :)",
});