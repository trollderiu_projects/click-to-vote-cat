
//poll url loading
$(document).ready(function () {
    setTimeout(function () {
        var keyId;
        var poll;

        //can't find url from device!
        if (window.screenPoll && "#polls" != location.hash) {
            poll = screenPoll;
            keyId = poll.key;
        } else {
            poll = new LoadedPoll();
            console.log("!screenPoll in GamePoll setTimeout");
        }

        if (!keyId) {
            console.log("!keyId");
            return;
        }

        console.log("GamePoll setTimeout " + keyId);
        var urlParts = getPathsFromKeyId(keyId);
        console.log(urlParts);

        if ("_" == urlParts.symbol) {
            console.log("'_' detected as GamePoll");
            window.gamePoll = new GamePoll("#pollsPage", keyId);
            //$("html").removeClass("withoutHeader");
            $("#body").addClass("pollsView");
        }
    }, 1);
});

var GamePoll = function (query, keyId) {
    console.log("GamePoll() " + keyId);
    this.coreSelect = "select.php";
    this.construct(query, keyId);
};

GamePoll.prototype.parsePolls = function (obj) {
    console.log(obj);
    var polls = [];

    var prefix = "";
    var lang = this.gameDB().split("_").pop();
    if ("private" != lang) {
        prefix = lang.toLowerCase();
    }

    for (var i = 0; i < obj.length; i++) {
        var row = obj[i];
        var data = row.data;
        console.log(data)
        var res = CSV.parseFirst(data);
        console.log(res)
        //all[row.id] = [row.id, row.id, res[1][0], res[1][1], row.answer0, row.answer1];
        //var key = convertBase(row.id, window.base10, window.base62);
        var key = keyId(row.id, lang);
        if (prefix) {
            key = prefix + "_" + key;
        }

        polls.push({
            key: key,
            id: row.id,
            t: row.timestamp,
            a0: res[1][0],
            a1: res[1][1],
            v0: row.v0 || 0,
            v1: row.v1 || 0
        });
    }

    return polls;
};


//DEVICE CALLBACK FUNCTION:
GamePoll.prototype.game_configCallback = function (json) {
    var data;
    if ("string" == typeof json) {
        try {
            data = JSON.parse(json);
        } catch (e) {
            console.log(json);
            console.log(e);
            return;
        }
    } else {
        data = json;
    }
    console.log("game_configCallback " + json);

    for (var key in data) {
        window[key] = data[key];
    }
};

//
extend(GamePoll, Polls);
